module.exports = {
    entry: "./ui/index.js",
    output: {
        path: __dirname + "/public/assets",
        filename: "bundle.js"
    },
    devtool: "eval-source-map",
    debug: true,
    module: {
        loaders: [
            { 
                test    : /\.js$|.jsx$/, 
                exclude : /(node_modules)/, 
                loader  : "babel",
                query: {
                    presets: ["es2015", "stage-0", "react"],
                }
            },
            { 
                test    : /\.scss$|.css$/,
                loader  : "style!css!sass",
            },
            {
                test: /\.eot/,
                loader: "url-loader?mimetype=application/vnd.ms-fontobject"
            },
            {
                test: /\.ttf/,
                loader: "url-loader?mimetype=application/x-font-ttf"
            },
            {
                test: /\.woff/,
                loader: "url-loader?mimetype=application/font-woff"
            },
            {
                test: /\.woff2/,
                loader: "url-loader?mimetype=application/font-woff2"
            },
            {
                test: /\.svg/,
                loader: "url-loader?mimetype=image/svg+xml"
            }
        ]
    },
    resolve: {
        extensions         : ["", ".js", ".jsx", ".json"],
        modulesDirectories : ["ui/styles", "ui/src", "node_modules"],
    }
}