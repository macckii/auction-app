require("colors")
var util = require("util")
var mongoose = require("mongoose")
var AuctionItem = require("../models/AuctionItem")
var User = require("../models/User")


mongoose.Promise = Promise

var data = [
    {
        "name": "Bicycle",
        "bought": false,
        "startDate": "2017-01-03 19:00",
        "endDate": "2017-04-30 21:00",
        "minimumPrice": 100000,
        "buyNowPrice": 200000,
        "image": "bicycle.jpg",
        "description": "I bought this bike 2 years ago. I only rode it for a few months last summer, so it still looks brand new.",
        "tags": ["sports", "transportation"],
        "sellerEmail": "finn@toughtootinbaby.com",
    },
    {
        "name": "Teapot",
        "bought": false,
        "startDate": "2016-12-05 9:00",
        "endDate": "2017-05-13 22:40",
        "minimumPrice": 500,
        "buyNowPrice": 1200,
        "image": "teapot.jpg",
        "description": "I got this teapot as a gift and never used it. It comes with a box and a mini book about how to infuse tea.",
        "tags": ["kitchen", "tea"],
        "sellerEmail": "pb@candykingdom.gov",
    },
    {
        "name": "Frying pan",
        "bought": false,
        "startDate": "2017-02-02 22:00",
        "endDate": "2017-04-21 0:00",
        "minimumPrice": 300,
        "buyNowPrice": 800,
        "image": "fryingpan.jpg",
        "description": "Used, but in good condition. I bought this pan a few months ago and I've only cooked bacon pancakes in it a few times.",
        "tags": ["kitchen"],
        "sellerEmail": "jtdoggzone@woof.net",
    }
]

function dropAuctionItemCollection() {
    console.info("Dropping Auction item collection...".cyan)
    return new Promise((resolve, reject) => {
        AuctionItem.collection.drop((err) => {
            if (err) console.error(util.inspect(err).magenta)
            console.log("Auction items collection dropped.".yellow)
            resolve()
        })
    })
}


module.exports = () => {
    return new Promise((resolve, reject) => {

        dropAuctionItemCollection()
        .then(() => {
            var totalDataCount = data.length
            var dataCount = 0
            data.forEach(item => {
                // delete seller email from each auction item
                var sellerEmail = item.sellerEmail
                delete item.sellerEmail

                // save each auction item
                var newAuctionItem = new AuctionItem(item)
                newAuctionItem
                .save()
                .then(doc => {
                    // find and update seller with auction item
                    var query = { "email": sellerEmail }
                    return User.findOneAndUpdate(query, { $push: {auctionItems: doc._id} }, { new: true })
                })              
                .then(user => {
                    if (user) {
                        console.info("Found user, %s".cyan, user.name)
                        // add seller to auction item based on seller's email
                        newAuctionItem._seller = user._id
                        return newAuctionItem.save()
                    } else {
                        throw "Unable to find user with email " + sellerEmail
                    }
                })
                .then(() => {
                    dataCount++
                    if (dataCount === totalDataCount) resolve()
                })
                .catch(err => reject(err))
            })
        })
    })
}

