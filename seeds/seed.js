var userSeed = require("./user")
var auctionItemSeed = require("./auction_item")
var config = require("config")
var mongoose = require("mongoose")

var db = mongoose.connect(config.get("database"))

// seed user first, then seed auction item
userSeed()
.then(() => {
  	return auctionItemSeed()
})
.catch(err => {
  	console.error(err)
})
.then(() => {
  	db.disconnect()
})
