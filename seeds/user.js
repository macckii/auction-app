require("colors")
var util = require("util")
var mongoose = require("mongoose")
var User = require("../models/User")

var data = [
    {
        "name": "Finn Mertens",
        "intro": "I'm a rough and tumble kind of guy who likes natchy outdoor livin'.",
        "email": "finn@toughtootinbaby.com",
        "image": "finn.png",
    },
    {
        "name": "Princess Bubblegum",
        "intro": "Benevolent semi-decocratic ruler by day, DJ by night.",
        "email": "pb@candykingdom.gov",
        "image": "pb.png",
    },
    {
        "name": "Jake the Dog",
        "intro": "Woof. Hehe.",
        "email": "jtdoggzone@woof.net",
        "image": "jake.png",
    },
]

module.exports = () => {
    return new Promise((resolve, reject) => {
        // First drop the collection...
        User.collection.drop(function(err) {
            if (err) console.error(util.inspect(err).magenta)
            console.log("Users collection dropped.".yellow)

            // ...then seed
            User.collection.insertMany(data, function(err, result) {
                if (err) console.error(err)
                console.log("%s document(s) are inserted.".yellow, result.insertedCount)
                resolve()
            })
        })
    })

}

