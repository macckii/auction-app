module.exports = {
    "database": process.env.MONGODB_CONNECTION,
    "port": process.env.PORT,
    "host": "localhost",
    "api_address": "https://arcane-reef-64244.herokuapp.com"
}