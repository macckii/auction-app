module.exports = {
    "database": "mongodb://localhost:27017/auction_app",
    "port": "3333",
    "host": "localhost",
    "api_address": "http://localhost:3333"
}