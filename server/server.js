var app = require("./app")
var config = require("config")
var mongoose = require("mongoose")

var db = mongoose.connect(config.get("database"))
app.listen(config.get("port"))