var express = require("express")
var cors = require("cors")
var bodyParser = require("body-parser")
var _ = require("lodash")
var util = require("util")

var app = express()
var http = require("http").Server(app)

var mainCtrl = require("./routes/web/main")
var auctionItemsCtrl = require("./routes/api/auctionItems")

var webSockets = require("./services/webSockets")

// Web sockets
webSockets.init(http)

// Views
app.set("views", "views")
app.engine("html", require("ejs").renderFile)
app.set("view engine", "ejs")

// Statics
app.use(express.static("public"))

// CORS
app.use(cors())

// Request parsing
app.use(bodyParser.json())

// Error handling
app.use((err, req, res, next) => {
    console.error(err)
})

// Web routes
app.get("/", mainCtrl.home)

// API routes
app.get("/auction_items", auctionItemsCtrl.get)
app.put("/auction_items/:id", auctionItemsCtrl.put)
app.post("/auction_items/:id/time_left", auctionItemsCtrl.post)

module.exports = http