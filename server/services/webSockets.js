var io

var socketIntervals = []

function emitTimeRemaining(id, endTime) {
    var now = new Date().getTime()
    var remainingTime = new Date(endTime).getTime() - now <= 0 ? 0 : new Date(endTime).getTime() - now
    io && io.sockets.emit("time:remaining:" + id, remainingTime)
}

module.exports = {

    init(http) {
        io = require("socket.io")(http)
        io.set("transports", ["websocket", "polling"])
        io.set("polling duration", 10)

        io.on("connection", (socket) => {
            socket.on("disconnect", function() {
                socketIntervals.forEach((socketInterval) => {
                    clearInterval(socketInterval)
                })
            })
        })
    },

    // Decrement countdown to end of auction every five seconds
    setupTimeLeftEmitter(id, endTime) {
        if (new Date(endTime).getTime() > new Date().getTime()) {
            emitTimeRemaining(id, endTime)
            
            socketIntervals.push(setInterval(function() {
                emitTimeRemaining(id, endTime)
            }, 5000))
        }
    },

}