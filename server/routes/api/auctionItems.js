var AuctionItem = require("../../../models/AuctionItem")
var User = require("../../../models/User")
var webSockets = require("../../services/webSockets")

module.exports = {

    get(req, res, next) {
        var success = docs => res.send(docs)

        AuctionItem
        .find()
        .lean()
        .then(docs => {
            return new Promise((resolve, reject) => {
                var totalResultCount = docs.length
                var resultCount = 0
                docs.forEach(doc => {
                    // find and embed seller object to each auction item
                    User.findById(doc._seller)
                    .then(user => {
                        doc.seller = user
                        resultCount++
                        if (resultCount === totalResultCount) resolve(docs)
                    })
                })
            })
        }, next)
        .then(success, next)
    },

    put(req, res, next) {
        var query = { "_id": req.params.id }

        AuctionItem
        .findOne(query)
        .then(doc => {
            doc = doc.toObject()

            var errors = {}
            if (doc.bought) {
                errors.bought = {
                    message: "The auction item is already sold."
                }
            }
            if (req.body.currentPrice) {
                const newCurrentPrice = req.body.currentPrice
                if (isNaN(newCurrentPrice)) {
                    errors.currentPrice = {
                        message: "Bid price must be a number."
                    }
                } 
                if (newCurrentPrice < doc.currentPrice) {
                    errors.currentPrice = {
                        message: "Bid price must be higher than current price."
                    }
                } 
                if (newCurrentPrice < doc.minimumPrice) {
                    errors.currentPrice = {
                        message: "Bid price must be higher than minimum price."
                    }
                } 
                if (newCurrentPrice >= doc.buyNowPrice) {
                    errors.currentPrice = {
                        message: "Bid price must be lower than buy now price."
                    }
                } 
            }

            if (Object.keys(errors).length) {
                res.status(400).send({ errors: errors })
            } else {
                var update = { $set: req.body }
                var opts = { new: true }
                var success = updatedDoc => { res.status(200).send({ auctionItem: updatedDoc }) }
                
                AuctionItem
                .findOneAndUpdate(query, update, opts)
                .then(success, next)
            }
        }, next)
    },

    post(req, res, next) {
        var query = { "_id": req.params.id }

        AuctionItem
        .findOne(query)
        .then(doc => {
            webSockets.setupTimeLeftEmitter(doc._id, doc.endDate)
            res.send(doc)
        }, next)
    },
}