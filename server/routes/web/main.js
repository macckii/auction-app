var config = require("config")

module.exports = {
    home(req, res, next) {
        res.render("index.html", { apiAddress: config.get("api_address") })
    }
}   