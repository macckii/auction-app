Auction Website
===============

Why did I make this?
--------------------
I made this auction website to demonstrate my ability to write both server-side and client-side Javascript. The server is ExpressJS and the client is React. I've used modern Javascript - ES2015 and beyond - whenever possible. The client-side is built with Webpack.

I used two different testing frameworks: Mocha and CasperJS. I used Mocha for api testing and CasperJS for UI testing.

You can find out more about me on [my LinkedIn page](https://au.linkedin.com/in/maki-sugita-2a1ab925).


See it live
-----------
[https://arcane-reef-64244.herokuapp.com](https://arcane-reef-64244.herokuapp.com)


Running tests
-------------

- API tests: `npm run test:api`
- UI tests: First start the server with `npm run server:test` and then `npm run test:ui`