casper.test.begin("Testing Auction website", 3, function(test) {
    casper.start("http://localhost:2222")

    casper.waitForSelector("#Bicycle", function() {
        casper.sendKeys("#Bicycle .bidPrice input", "1200")
        casper.click("#Bicycle .bidPrice button")
        test.assertTextExists("Bid 1200.00 for this item?", "Correct confirm message")
        casper.click("#Bicycle .yes")
    })

    casper.waitForText("You are currently the highest bidder.", function() {
        test.assert(true, "Highest bidder message appears")
        test.assertTextExists("Current Price: 1200.00", "Correct bid price")
    })

    casper.run(function() {
        test.done()
    })
})

