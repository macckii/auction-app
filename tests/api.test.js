process.env.NODE_ENV = "test"

var superagent = require("superagent")
var expect = require("expect.js")
var mongoose = require("mongoose")
var config = require("config")
var User = require("../models/User")
var AuctionItem = require("../models/AuctionItem")
var auctionItemSeed = require("../seeds/auction_item")
var userSeed = require("../seeds/user")
var app = require("../server/app")
var util = require("util")

var db = mongoose.connect(config.get("database"))

const HOST = config.get("host")
const PORT = config.get("port")
const API  = `http://${HOST}:${PORT}`


describe("Testing api", function() {

    before(function(done) {
        app.listen(PORT)

        // seed db
        userSeed()
        .then(() => {
            return auctionItemSeed()
        })
        .catch(err => {
            console.error(err)
        })
        .then(() => {
            done()
        })
    })

    after(function() {
        db.disconnect()
    })

    describe("Auction item", function() {
        var bicycle
        var bidder = "user1@test.com"
        var failedBidder = "user2@test.com"

        before(function(done) {
            AuctionItem
            .findOne({ name: "Bicycle" })
            .then(function(result) {
                bicycle = result
                done()
            })
        })

        it("retrieves auction items", function(done) {
            superagent
            .get(API + "/auction_items")
            .end(function(err, res) {
                expect(err).to.eql(null)
                expect(res.body.length).to.eql(3)
                done()
            })
        })

        it("bids on an auction item successfully", function(done) {
            superagent
            .put(API + "/auction_items/" + bicycle._id)
            .send({
                bidderEmail: bidder,
                currentPrice: 150000
            })
            .end(function(err, res) {
                expect(err).to.eql(null)
                expect(res.status).to.eql(200)

                var auctionItem = res.body.auctionItem
                
                expect(auctionItem.bidderEmail).to.eql(bidder)
                expect(auctionItem.currentPrice).to.eql(150000)
                done()
            })
        })

        it("fails when bidding on an auction item with a lower price than minimum price", function(done) {
            superagent
            .put(API + "/auction_items/" + bicycle._id)
            .send({
                bidderEmail: failedBidder,
                currentPrice: 90000
            })
            .end(function(err, res) {
                expect(res.status).to.eql(400)

                var errors = res.body.errors
                expect(errors.currentPrice.message).to.eql("Bid price must be higher than minimum price.")
                done()
            })
        })

        it("fails when bidding on an auction item with a lower price than current price", function(done) {
            superagent
            .put(API + "/auction_items/" + bicycle._id)
            .send({
                bidderEmail: failedBidder,
                currentPrice: 140000
            })
            .end(function(err, res) {
                expect(res.status).to.eql(400)

                var errors = res.body.errors
                expect(errors.currentPrice.message).to.eql("Bid price must be higher than current price.")
                done()
            })
        })

        it("fails when bidding on an auction item with an invalid value (e.g. string)", function(done) {
            superagent
            .put(API + "/auction_items/" + bicycle._id)
            .send({
                bidderEmail: failedBidder,
                currentPrice: "160000 dollars"
            })
            .end(function(err, res) {
                expect(res.status).to.eql(400)

                var errors = res.body.errors
                expect(errors.currentPrice.message).to.eql("Bid price must be a number.")
                done()
            })
        })

        it("perform buy now on an auction item successfully", function(done) {
            superagent
            .put(API + "/auction_items/" + bicycle._id)
            .send({
                bidderEmail: bidder,
                bought: true
            })
            .end(function(err, res) {
                expect(err).to.eql(null)
                expect(res.status).to.eql(200)

                var auctionItem = res.body.auctionItem
                
                expect(auctionItem.bidderEmail).to.eql(bidder)
                expect(auctionItem.bought).to.eql(true)
                done()
            })
        })

        it("fails when bidding on an closed auction item", function(done) {
            superagent
            .put(API + "/auction_items/" + bicycle._id)
            .send({
                bidderEmail: failedBidder,
                currentPrice: 190000
            })
            .end(function(err, res) {
                expect(res.status).to.eql(400)

                var errors = res.body.errors
                expect(errors.bought.message).to.eql("The auction item is already sold.")
                done()
            })
        })

    })

})