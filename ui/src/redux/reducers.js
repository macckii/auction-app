import {
    REQUEST_AUCTION_ITEMS, 
    RECEIVE_AUCTION_ITEMS, 
    UPDATE_FILTER_TAGS,
    UPDATE_SEARCH_TERM } from "./actions"

const initialState = {
    items: [],
    fetching: false,
    filterTags: [],
    searchTerm: "",
}

const auctionItems = (state = initialState, action) => {
    switch(action.type) {
        case REQUEST_AUCTION_ITEMS:
            return {
                items: action.items,
                fetching: true,
                filterTags: action.filterTags,
                searchTerm: action.searchTerm,
            }
        case RECEIVE_AUCTION_ITEMS:
        case UPDATE_FILTER_TAGS:
        case UPDATE_SEARCH_TERM:
            return {
                items: action.items,
                fetching: false,
                filterTags: action.filterTags,
                searchTerm: action.searchTerm,
            }
        default:
            return state
    }
}

export default auctionItems