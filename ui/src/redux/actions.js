import api from "api"

export const REQUEST_AUCTION_ITEMS = "REQUEST_AUCTION_ITEMS"
export const RECEIVE_AUCTION_ITEMS = "RECEIVE_AUCTION_ITEMS"
export const UPDATE_FILTER_TAGS = "UPDATE_FILTER_TAGS"
export const UPDATE_SEARCH_TERM = "UPDATE_SEARCH_TERM"

const createFilterTags = (auctionItems) => {
    var allTags = []
    auctionItems.forEach(item => {
        allTags = allTags.concat(item.tags)
    })
    var tagCount = {}
    allTags.forEach(tag => { tagCount[tag] = (tagCount[tag] || 0) + 1 })

    var filterTags = []
    for (var key in tagCount) {
        var tag = {
            name: key,
            count: tagCount[key],
            selected: false
        }
        filterTags.push(tag)
    }
    return filterTags
}

const requestAuctionItems = () => ({
    type: REQUEST_AUCTION_ITEMS,
    items: [],
    filterTags: [],
    searchTerm: "",
})

const receiveAuctionItems = (auctionItems) => {
    return {
        type: RECEIVE_AUCTION_ITEMS,
        items: auctionItems,
        filterTags: createFilterTags(auctionItems),
        searchTerm: "",
    }
}

const updateFilterTags = filterTags => (dispatch, getState) => {
    const { items, searchTerm } = getState()
    dispatch(
        {
            type: UPDATE_FILTER_TAGS,
            items: items,
            filterTags: filterTags,
            searchTerm: searchTerm,
        }
    )
}

export const updateSearchTerm = searchTerm => (dispatch, getState) => {
    const { items, filterTags } = getState()
    dispatch(
        {
            type: UPDATE_SEARCH_TERM,
            items: items,
            filterTags: filterTags,
            searchTerm: searchTerm,
        }
    )
}

export const fetchAuctionItems = () => dispatch => {
    dispatch(requestAuctionItems())
    return api.get("/auction_items").end((err, res) => {
        if (err || !res.ok) {
            console.error(err) // TODO: Handle error for user
        } else {
            dispatch(receiveAuctionItems(res.body))
        }
    })
}

export const updateTagStatus = nextTag => (dispatch, getState) => {
    const { filterTags } = getState()
    var updatedFilterTags = filterTags.map(tag => {
        if (tag.name === nextTag.name) {
            tag.selected = nextTag.selected
        }
        return tag
    })
    dispatch(updateFilterTags(updatedFilterTags))
}