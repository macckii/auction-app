import { apiAddress } from "api"

const socket = io.connect(apiAddress)

export default socket