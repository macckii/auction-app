import React, { Component, PropTypes } from "react"
import Modal from "components/Modal"
import { apiAddress } from "api"

export default class SellerModal extends Component {

    static propTypes = {
        seller: PropTypes.object.isRequired,
        open: PropTypes.bool.isRequired,
        onClose: PropTypes.func.isRequired,
    }

	render() {
        var { seller } = this.props
    
        // TODO: Display auction items
        var itemList = seller.auctionItems.map(item => {
            return (
                <li>
                    <figure>
                        <img src={ `${apiAddress}/images/${item.image}` } alt={ item.name } />
                        <figcaption>{ item.name }</figcaption>
                    </figure>
                </li>
            )
        })
		
		return (
			<Modal
				open={ this.props.open }
				onClose={ this.props.onClose }>
				<div className="row">
					<div className="col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <div className="author-img"><img src={ `${apiAddress}/images/users/${seller.image}` } alt={ seller.name } /></div>
                        <h2>{ seller.name }</h2>
                        <p>{ seller.intro }</p>
					</div>
				</div>
			</Modal>
		)
	}
}