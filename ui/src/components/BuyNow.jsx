import React, { Component, PropTypes } from "react"
import ReactDOM from "react-dom"
import { bindAll } from "lodash"
import ConfirmButton from "components/ConfirmButton"
import Loading from "components/Loading"
import api from "api"
import { connect } from "react-redux"
import { fetchAuctionItems } from "redux/actions"

const CONFIRM = "confirm"
const LOADING = "loading"
const BUTTON  = "button"

class BuyNow extends Component {

    static propTypes = {
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.string.isRequired,
        dispatch: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props)
        bindAll(this, ["confirmBuyNow", "cancelBuyNow", "handleBuyNow"])
        this.state = { view: BUTTON }
    }

    confirmBuyNow() {
        this.setState({ view: CONFIRM })
    }

    cancelBuyNow() {
        this.setState({ view: BUTTON })
    }

    handleBuyNow() {
        this.setState({ view: LOADING })
        const { id, name, price, dispatch } = this.props

        api
        .put(`/auction_items/${id}`)
        .send({ bought: true })
        .end((err, res) => {
            if (err || !res.ok) {
                // TODO: Improve error handling UI
                window.alert(err)
                this.setState({ view: BUTTON })
            } else {
                // TODO: Improve UI
                window.alert(`You bought ${name} at $${price}!`)
                dispatch(fetchAuctionItems())
            }
        })
    }

    render() {
        var content

        const { price } = this.props

        switch (this.state.view) {
            case CONFIRM:
                content = <ConfirmButton confirm={ this.handleBuyNow } cancel={ this.cancelBuyNow } msg="Do you really want to buy this item now?" />
                break
            case LOADING:
                content = <Loading />
                break
            case BUTTON:
                content = <button className="btn btn-primary" onClick={ this.confirmBuyNow } style={{ width: "100%" }}>Buy Now for ${ price }</button>
                break
            default:
        }
        
        return content
    }

}

const mapStateToProps = state => {
    return state
}

export default connect(mapStateToProps)(BuyNow)