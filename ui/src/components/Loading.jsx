import React, { Component } from "react"

export default class Loading extends Component {

    render() {
        return <span className="glyphicon glyphicon-refresh spinning"></span>
    }
}