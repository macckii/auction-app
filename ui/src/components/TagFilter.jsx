import React, { Component, PropTypes } from "react"
import { connect } from "react-redux"
import { updateTagStatus } from "redux/actions"
import cx from "classnames"


class Tag extends Component {

    static propTypes = {
        dispatch: PropTypes.func.isRequired,
    }

    handleClick() {
        const { dispatch, tag } = this.props 
        const nextTag = {
            name: tag.name,
            selected: !tag.selected,
        }
        dispatch(updateTagStatus(nextTag))
    }

    render() {
        const { tag } = this.props

        const classes = cx({
            "tag-filter--link": true,
            "tag-filter--link--selected": tag.selected
        })

        return (
            <a 
                name={ tag.name } 
                onClick={ () => this.handleClick() }
                className={classes}>
                <span>
                    { tag.selected ? <span>&#x2714; </span> : null }
                    { tag.name }
                </span>
                <span className="count">{ tag.count }</span>
            </a>
        )
    }
}


class TagFilter extends Component {

    static propTypes = {
        tags: PropTypes.array.isRequired,
        dispatch: PropTypes.func.isRequired,
    }

    render() {

        const tags = this.props.tags.map(tag => {
            return (
                <li key={ tag.name + "_tag_filter" }>
                    <Tag tag={ tag } dispatch={ this.props.dispatch } />
                </li>
            )
        })

        return (
            <div className="tag-filter">
                <ul>{ tags }</ul>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        tags: ownProps.tags,
    }
}

export default connect(mapStateToProps)(TagFilter)