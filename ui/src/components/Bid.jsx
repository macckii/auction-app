import React, { Component, PropTypes } from "react"
import { bindAll } from "lodash"
import ConfirmButton from "components/ConfirmButton"
import Loading from "components/Loading"
import api from "api"
import { connect } from "react-redux"
import { fetchAuctionItems } from "redux/actions"

const CURRENT_HIGHEST_BID  = "current highest bid"
const CONFIRM = "confirm"
const LOADING = "loading"
const BUTTON  = "button"

const BidInput = (props) => {
    const { handleChange, bidPrice, confirm, minimumBid } = props
    const disabled = bidPrice === "" ? true : parseInt(bidPrice, 10) < minimumBid

    return (
        <div className="bidPrice input-group input-group-lg">
            <input type="number" step="1" onChange={ handleChange } value={ bidPrice } className="form-control" />
            <span className="input-group-btn">
                <button className="btn btn-primary"  disabled={ disabled } onClick={ confirm }>Bid</button>
            </span>
        </div>
    )
}

class Bid extends Component {

    static propTypes = {
        id: PropTypes.string.isRequired,
        minimumBid: PropTypes.number,
        dispatch: PropTypes.func.isRequired
    }

    static defaultProps = {
        minimumBid: 0,
    }

    constructor(props) {
        super(props)
        bindAll(this, ["handleChange", "confirmBid", "cancelBid", "handleBid"])
        this.state = { bidPrice: "", view: BUTTON }
    }

    handleChange(e) {
        this.setState({ bidPrice: e.target.value })
    }

    confirmBid() {
        this.setState({ view: CONFIRM })
    }

    cancelBid() {
        this.setState({ view: BUTTON })
    }

    handleBid() {
        this.setState({ view: LOADING })

        const { id, dispatch } = this.props

        api
        .put(`/auction_items/${id}`)
        .send({ currentPrice: Number(this.state.bidPrice) * 100 })
        .end((err, res) => {
            if (err || !res.ok) {
                console.log(err)
                this.setState({ view: BUTTON })
            } else {
                this.setState({ view: CURRENT_HIGHEST_BID })
                // onBidSucceeded(res.body.auctionItem)
                dispatch(fetchAuctionItems())
            }
        })
    }

    render() {
        var content

        const { view, bidPrice } = this.state
        const { minimumBid } = this.props

        switch (view) {
            case CURRENT_HIGHEST_BID:
                content = <p>You are currently the highest bidder.</p>
                break
            case CONFIRM:
                content = <ConfirmButton confirm={ this.handleBid } cancel={ this.cancelBid } msg={ `Bid ${Number(bidPrice).toFixed(2)} for this item?` } />
                break
            case LOADING:
                content = <Loading />
                break
            case BUTTON:
                content = <BidInput handleChange={ this.handleChange } bidPrice={ bidPrice } confirm={ this.confirmBid } minimumBid={ minimumBid } />
                break
            default:
        }

        return content
    }
}


const mapStateToProps = state => {
    return state
}

export default connect(mapStateToProps)(Bid)