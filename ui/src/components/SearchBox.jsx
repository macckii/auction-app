import React, { Component, PropTypes } from "react"
import TagFilter from "components/TagFilter"
import { connect } from "react-redux"
import { updateSearchTerm } from "redux/actions"

class SearchBox extends Component {

    static propTypes = {
        items: PropTypes.array.isRequired,
        dispatch: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props)
        this.state = { input: "", suggestions: [] }
    }

    handleChange(e) {
        var input = e.target.value
        this.setState({ input: input })
    }

    handleSearchSubmit() {
        this.props.dispatch(updateSearchTerm(this.state.input))
    }

    handleKeyDown(e) {
        if (e.keyCode === 13) this.handleSearchSubmit()
    }

    getSuggestions() {
        var suggestions = []
        this.props.items.forEach(item => {
            var itemName = item.name.toLowerCase()
            var input = this.state.input.toLocaleLowerCase()
            if (itemName.startsWith(input)) {
                suggestions.push(<option key={ item.name + "_suggestion_option" } label={ item.name } value={ item.name } />)
            }
        })     
        return suggestions
    }

    render() {
        const suggestions = this.getSuggestions()
        return (
            <div className="input-group search-box">
                <input 
                    type="text" 
                    className="form-control" 
                    placeholder="Search for..."
                    onKeyDown={ (e) => this.handleKeyDown(e) }
                    onChange={ (e) => this.handleChange(e) }
                    list="suggestions" />
                <datalist id="suggestions">{ suggestions }</datalist>
                <span className="input-group-btn">
                    <button className="btn btn-default" type="button" onClick={ () => this.handleSearchSubmit() }>
                        <span className="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </button>
                </span>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        items: state.items,
     }
}

export default connect(mapStateToProps)(SearchBox)