import React, { Component, PropTypes } from "react"
import request from "superagent"
import Bid from "components/Bid"
import BuyNow from "components/BuyNow"
import Duration from "components/Duration"
import SellerModal from "components/SellerModal"
import { currency, dateFormat } from "formatting"
import { apiAddress } from "api"
import { connect } from "react-redux"

const PurchaseControls = (props) => {
    const { item } = props

    var bidInfo

    if (item.currentPrice != null) {
        bidInfo = <p><b>Current Bid:</b> ${ currency(item.currentPrice) }</p>
    } else {
        bidInfo = <p>Starting Bid: ${ currency(item.minimumPrice) }</p>
    }

    return (
        <div>
            { bidInfo }
            <div style={ {marginBottom: "10px"} }>
                <Bid 
                    id={ item._id } 
                    minimumBid={ (item.currentPrice || item.minimumPrice) / 100 }/>
            </div>
            <BuyNow 
                id={ item._id } 
                name={ item.name } 
                price={ currency(item.buyNowPrice) } />
        </div>
    )
}

class AuctionItem extends Component {

    static propTypes = {
        item: PropTypes.object.isRequired,
        filterTags: PropTypes.array.isRequired,
        dispatch: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props)
        this.state = { openSellerModal: false }
        this.setTimeLeft()
    }

    setTimeLeft() {
        const { item } = this.props
        if (!item.bought) {
            request
            .post(`/auction_items/${item._id}/time_left`)
            .end((err, res) => {
                if (err || !res.ok) {
                    console.error(err) // TODO: Display in UI
                }
            })
        }
    }

    getBidSection() {
        const { item } = this.props

        var content

        const expired = new Date(item.endDate) <= new Date()
        
        if (item.bought || expired) {
            content = <p><big><b>Closed</b></big></p>
        } else {
            content = (         
                <div>
                    <p className="auction-item--time-left">
                        Ends <Duration id={ item._id } />
                    </p>
                    <PurchaseControls item={ item } />
                </div> 
            )
        }

        return content
    }

    render() {
        const { item, filterTags } = this.props
        const { openSellerModal } = this.state

        const tagNames = filterTags.filter(tag => item.tags.indexOf(tag.name) !== -1)
                            .map(tag => tag.name)
                            .join(", ")

        const bidSection = this.getBidSection()

        return (
            <div className="row auction-item" id={ item.name }>
                
                <SellerModal 
                    open={ openSellerModal } 
                    seller={ item.seller } 
                    onClose={ () => this.setState({ openSellerModal: false }) } />

                <div className="col-md-3 auction-item--img">
                    <img className="img-responsive img-thumbnail" src={ `${apiAddress}/images/${item.image}` } alt={ item.name } />
                </div> 

                <div className="col-md-6 auction-item--info">
                    <h2 className="auction-item--title">{ item.name }</h2>
                    <p className="auction-item--seller">Seller: <a onClick={ () => this.setState({ openSellerModal: true }) }>{ item.seller.name }</a></p>
                    <p>{ item.description }</p>
                    <div>Tags: { tagNames }</div>
                </div>

                <div className="col-md-3 auction-item--bid">
                    { bidSection }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return { filterTags: state.filterTags }
}

export default connect(mapStateToProps)(AuctionItem)