import React, { Component, PropTypes } from "react"
import ReactDOM from "react-dom"
import moment from "moment"
import socket from "socket"

const oneDay = 1000 * 60 * 60 * 24
const oneHour = 1000 * 60 * 60
const oneMinute = 1000 * 60
const oneSecond = 1000

const getDays = (m) => Math.floor(m / oneDay)
const getHours = (m) => Math.floor(m / oneHour)

const TimeRemaining = ({ milliseconds }) => {
    var result

    if (milliseconds >= (oneDay * 2)) {
        result = `in ${getDays(milliseconds)} days`
    } else {

        let now = moment()
        let endTime = moment(now.valueOf() + milliseconds)

        if (now.dayOfYear() == endTime.dayOfYear() - 1) {
            result = `tomorrow at ${endTime.format("h:mm a")}`
        } else {
            result = `in ${getHours(milliseconds)} hours`
        }
    }

    return <span>{result}</span>
}

const CountDown = ({ milliseconds }) => {
    const days = getDays(milliseconds)
    const hours = getHours(milliseconds - days * oneDay)
    const minutes = Math.floor((milliseconds - days * oneDay - hours * oneHour) / oneMinute)
    const seconds = Math.floor((milliseconds - days * oneDay - hours * oneHour - minutes * oneMinute) / oneSecond)

    return <span>in <big>{ hours }</big>h <big>{ minutes }</big>m <big>{ seconds }</big>s</span>
}

export default class Duration extends Component {

    constructor(props) {
        super(props)
        this.state = { milliseconds: 0 }
    }

    static propTypes = {
        id: PropTypes.string.isRequired,
    }

    componentDidMount() {
        this.socketHandler = (timeLeft) => {
            this.setState({ milliseconds: timeLeft })
        }
        socket.on(`time:remaining:${this.props.id}`, this.socketHandler)
    }

    componentWillUnmount() {
        socket.removeListener(`time:remaining:${this.props.id}`, this.socketHandler)
    }

    render() {
        const { milliseconds } = this.state

        const C = getHours(milliseconds) < 2 ? CountDown : TimeRemaining

        return <C milliseconds={milliseconds} />
    }
}