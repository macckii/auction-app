import React, { Component, PropTypes } from "react"
import Modal from "components/Modal"
import { apiAddress } from "api"

export default class IntroModal extends Component {

	static propTypes = {
        open: PropTypes.bool.isRequired,
        onClose: PropTypes.func.isRequired,
    }

	render() {
		return (
			<Modal
				open={ this.props.open }
				onClose={ this.props.onClose }>
				<div className="row">
					<div className="col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
						<div className="author-img"><img src={ `${apiAddress}/images/users/maki.jpg` } alt="Maki" /></div>
						<h2>Maki's Portfolio Work</h2>
						<p>Thank you for visiting my portfolio work!</p>
						<p>I'm Maki Sugita and I'm currently looking for a job as a software developer.</p>
						<p>Here are the remarkable points of this sample application:</p>
						<ol>
							<li><b>React:</b> Built with Webpack and using ES2016 whenever possible.</li>
							<li><b>Redux:</b> Client-side state management.</li>
							<li><b>Promises:</b> Checkout <i>/seeds</i> </li>
							<li><b>Web Sockets:</b> Using Socket.io</li>
							<li><b>Sass:</b> Modern, organized styles</li>
							<li><b>Testing:</b> End-to-end tests and API tests</li>
						</ol>
						<p>The source code is <a href="https://bitbucket.org/macckii/auction-app">here</a>.</p>
						<p>If you like my work, please contact me through <a href="https://www.linkedin.com/in/maki-sugita-2a1ab925">LinkedIn</a>!</p>
					</div>
				</div>
			</Modal>
		)
	}
}