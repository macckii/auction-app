import React, { Component, PropTypes } from "react"

export default class ConfirmButton extends Component {

    static propTypes = {
        msg: PropTypes.string.isRequired,
        confirm: PropTypes.func,
        cancel: PropTypes.func,
    }

    static defaultProps = {
        confirm: () => {},
        cancel: () => {},
    }

    handleCancel(e) {
        e.preventDefault()
        e.stopPropagation()

        this.props.cancel()
    }

    render() {
        return (
            <div>
                <p>{this.props.msg}</p>
                <ul className="list-inline">
                    <li><button className="btn btn-primary yes" onClick={this.props.confirm}>Yes</button></li>
                    <li><a className="cancel" onClick={(e) => this.handleCancel(e)} href="#">Cancel</a></li>
                </ul>
            </div>
        )
    }
}