import React, { Component, PropTypes } from "react"

export default class Modal extends Component {

	static propTypes = {
		open: PropTypes.bool.isRequired,
		onClose: PropTypes.func.isRequired,
	}

	componentDidMount() {
		if (this.props.open) this.refs.modalContent.focus()
	}

	componentDidUpdate() {
		if (this.props.open) {
			this.refs.modalContent.focus()
		}
	}

    render() {
		const { open, onClose, children } = this.props

		if (open) {
			return (
				<div className="modal--overlay">
					<div className="modal--message" onBlur={ onClose } tabIndex="0" ref="modalContent">
						<div className="modal--close-link">
							<a onClick={ onClose }>x</a>
						</div>
						{ children }
					</div>
				</div>
			)
		}

		return null
	}
}