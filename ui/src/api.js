import request from "superagent"

export const apiAddress = document.getElementsByTagName("head").item(0).getAttribute("data-api-address")

export default {
    
    get(path) {
        return request.get(apiAddress + path)
    },

    put(path, params = {}) {
        return request.put(apiAddress + path).send(params)
    }
}