import React, { Component, PropTypes } from "react"
import AuctionItem from "components/AuctionItem"
import SearchBox from "components/SearchBox"
import TagFilter from "components/TagFilter"
import IntroModal from "components/IntroModal"
import socket from "socket"
import { connect } from "react-redux"
import { fetchAuctionItems } from "redux/actions"


class App extends Component {

    static propTypes = {
        items: PropTypes.array.isRequired,
        filterTags: PropTypes.array.isRequired,
        searchTerm: PropTypes.string.isRequired,
        fetching: PropTypes.bool.isRequired,
        dispatch: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props)
        this.state = { openModal: true }
    }

    componentDidMount() {
        const { dispatch } = this.props
        dispatch(fetchAuctionItems())
    }

    componentWillUnmount() {
        socket.disconnect()
    }

    getSelectedTags() {
        const { filterTags } = this.props

        return filterTags.reduce((result, tag) => {
            if (tag.selected) result.push(tag.name)
            return result
        }, [])
    }

    getSearchResults() {
        const selectedTags = this.getSelectedTags()
        var items = []
        this.props.items.forEach(item => {
            let include = true
            
            // apply tag filter
            if (selectedTags.length !== 0) {
                include = item.tags.some(tag => {
                    return selectedTags.indexOf(tag) !== -1
                })
            }

            // apply search term
            if (this.props.searchTerm.length !== 0) {
                var itemName = item.name.toLowerCase()
                var searchTerm = this.props.searchTerm.toLowerCase()
                if (!itemName.startsWith(searchTerm)) include = false
            }

            if (include) items.push(<AuctionItem item={ item } key={ item._id } />)
        })

        return items
    }

    render() {
        const { filterTags } = this.props
        const searchResults = this.getSearchResults()

        return (
            <div>
                <h1>Ooo Auctions</h1>
                <p className="title--sub-description">A phony website by <a href="#" onClick={ () => this.setState({ openModal: true })}>Maki Sugita</a>.</p>
                <hr />
                
                <div className="row">
                    <div className="col-md-2">
                        <SearchBox />
                        <TagFilter tags={ filterTags } />
                    </div>
                    <div className="col-md-10">{ searchResults }</div>
                    <IntroModal open={ this.state.openModal } onClose={ () => this.setState({ openModal: false }) } />
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return state
}

export default connect(mapStateToProps)(App)