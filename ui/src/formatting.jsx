import dateformat from "dateformat"

export const currency = (value = 0) => {
    return (value / 100).toFixed(2)
}

export const dateFormat = (value, format = "yyyy-mm-dd H:MM") => {
    return dateformat(value, format)
}