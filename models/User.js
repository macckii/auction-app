var mongoose = require("mongoose")

var UserSchema = new mongoose.Schema({
    name: String,
    intro: String,
    email: { type: String, index: true, unique: true },
    image: String,
    auctionItems: [{ type: mongoose.Schema.ObjectId, ref: "AuctionItem" }]
})

var User = mongoose.model("users", UserSchema)

module.exports = User