var mongoose = require("mongoose")
var Schema = mongoose.Schema

var AuctionItemSchema = new Schema({
    name: String,
    bought: Boolean,
    startDate: Date,
    endDate: Date,
    minimumPrice: Number,
    buyNowPrice: Number,
    currentPrice: Number,
    bidderEmail: String,
    image: String,
    description: String,
    tags: Array,
    _seller: { type: Schema.Types.ObjectId, ref: "User" }
})

var AuctionItem = mongoose.model("auction_items", AuctionItemSchema)

module.exports = AuctionItem